import rabbit.*;
import java.util.*;
import java.io.*;

/*----------------------------------------------------------------------*
* Our main program                                                      *
*----------------------------------------------------------------------*/	
public class Main
{
	public static void main(String[] args) throws FileNotFoundException, IOException
	{
		Simulation sim = new Simulation(args[0]);
		sim.print();
		sim.iterate(args[1]);
	}
}
	
/*                              
         ,
        /|      __
       / |   ,-~ /
      Y :|  //  /
      | jj /( .^                                  __
      >-"~"-v"                           /\    .-" /
     /       Y                          /  ; .'  .' 
    jo  o    |                         :   :/  .'   
   ( ~T~     j                         \  ;-.'     
    >._-' _./             .--""""--..__/     `.    
   /   "~"  |           .'           .'    `o  \   
  Y     _,  |          /                    `   ;  
 /| ;-"~ _  l         :                  \      :  
/ l/ ,-"~    \      .-;        -.         `.__.-'  
\//\/      .- \    :  ;          \     ,   ;       
 Y        /    Y   '._:           ;   :   (        
 l       I     !       \/  .__    ;    \   `-.     
 ]\      _\    /"\      ;     "-,/_..--"`-..__)    
(" ~----( ~   Y.  )     '""--.._:
*/