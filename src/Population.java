package rabbit;
import java.util.*;

/*----------------------------------------------------------------------*
* Population : Represent a rabbit population                            *
*----------------------------------------------------------------------*/
public class Population
{
	private LinkedList<Rabbit> pop;
	private int adultMales;

	//Constants
	final static double UNIFORMPOP = 0.5;
	final static double DENSITY3 = 1;

	/*----------------------------------------------------------------------*
	* Constructor                                                           *
	*----------------------------------------------------------------------*/
	public Population()
	{
		this.pop = new LinkedList<Rabbit>();
	}

	/*----------------------------------------------------------------------*
   	* Getter adultMales                                                     *
   	*                                                                       *
   	* Output :                                                              *
   	*      - a int representing the number of adult male rabbit             *
   	*----------------------------------------------------------------------*/
	public int adultMales()
	{
		return this.adultMales;
	}

	/*----------------------------------------------------------------------*
    * get : get the index element of the population                         *
    *                                                                       *
    * Input :                                                               *
    *      - _i : an int used as index of element                           *
    *                                                                       *
   	* Output :                                                              *
  	*      - a char representing the sexe of the rabbit                     *
   	*----------------------------------------------------------------------*/
	public Rabbit get(int _i)
	{
		return this.pop.get(_i);
	}

	/*----------------------------------------------------------------------*
    * add : add a new element to the population                             *
    *                                                                       *
    * Input :                                                               *
    *      - _rab : an Rabbit used as new element                           *
   	*----------------------------------------------------------------------*/
	public void add(Rabbit _rab)
	{
		//Increment counter if new adutl male rabbit
		if(_rab.sexe() == 'm')
		{
			if(_rab.age() > _rab.ADULT)
			{
				this.adultMales++;
			}
		}

		this.pop.add(_rab);
	}

	/*----------------------------------------------------------------------*
    * remove : remove the target element of the population                  *
    *                                                                       *
    * Input :                                                               *
    *      - _rab : an Rabbit used as target element                        *
   	*----------------------------------------------------------------------*/
	public void remove(Rabbit _rab)
	{
		//DEcrement counter if adult male rabbit
		if(_rab.sexe() == 'm')
		{
			if(_rab.age() > _rab.ADULT)
			{
				this.adultMales = this.adultMales == 0 ? 0 : this.adultMales-1;
			}
		}

		this.pop.remove(_rab);
	}

   /*----------------------------------------------------------------------*
   * aging : make the population age by one year                           *
   *----------------------------------------------------------------------*/
	public void aging()
	{
		//For each rabbit
		for(int i  = 0 ; i < this.size() ; i++)
		{
			//If there is a new adult male rabbit, increment counter
			if(this.get(i).sexe() == 'm')
			{
				if(this.get(i).age() ==  Rabbit.ADULT-1)
				{
					this.adultMales++;

				}
			}

			this.get(i).aging();
		}
	}

	/*----------------------------------------------------------------------*
    * size : return the size of the population                              *
    *                                                                       *
   	* Output :                                                              *
  	*      - an int representing the size of the population                 *
   	*----------------------------------------------------------------------*/
	public int size()
	{
		return this.pop.size();
	}

	/*----------------------------------------------------------------------*
    * clone : return a clone af the rabbit list                             *
    *                                                                       *
   	* Output :                                                              *
  	*      - a linkedlist representing the population      v                *
   	*----------------------------------------------------------------------*/
	public LinkedList<Rabbit> clone()
	{
		LinkedList<Rabbit> clone = new LinkedList<Rabbit>();
		for(int i = 0 ; i < this.pop.size() ; i++)
		{
			clone.add(this.pop.get(i));
		}
		return clone;
	}
}