package rabbit;
import java.util.*;

/*---------------------------------------------------------------------*
* Parameter : represent all the environmentals parameters              *
*----------------------------------------------------------------------*/
public class Parameter
{
    //Area surface
    private double surface;

    //Array size
    private int climateSize;
    private int predaSize;
    private int diseaseSize;
    private int foodSize;

    //List of parameter
    private ArrayList<Double> food;
    private ArrayList<ArrayList<Integer>> climate;
    private ArrayList<Double> preda;
    private ArrayList<ArrayList<Double>>  disease;

    /*----------------------------------------------------------------------*
    * Constructor                                                           *
    *----------------------------------------------------------------------*/
    public Parameter()
    {
        this.food = new ArrayList<Double>();
        this.preda = new ArrayList<Double>();
    }

    /*----------------------------------------------------------------------*
    * initClimate : initialize a 2d array list representing the climate     *
    *----------------------------------------------------------------------*/    
    public void initClimate()
    {
        this.climate = new ArrayList<ArrayList<Integer>>();
        for(int i = 0 ; i < this.climateSize ; i++)
        {
            this.climate.add(new ArrayList<Integer>());
        }
    }

    /*----------------------------------------------------------------------*
    * initDisease : initialize a 2d array list representing the diseases    *
    *----------------------------------------------------------------------*/  
    public void initDisease()
    {
        this.disease = new ArrayList<ArrayList<Double>>();
        for(int i = 0 ; i < this.diseaseSize ; i++)
        {
            this.disease.add(new ArrayList<Double>());
        }
    }

    /*----------------------------------------------------------------------*
    * PrintParameter : print the Paramter struct in terminal               *
    *                                                                      *
    * Input :                                                              *
    *      - _para : a pointer to the Parameter struct we want to print    *
    *                                                                      *
    * Output : void                                                        *
    *----------------------------------------------------------------------*/
    public void print()
    {
        System.console().printf("Food modifier:\n");

        //Process through the food array
        for(int i = 0 ; i < this.foodSize; i++)
        {
            System.console().printf("Food %d, modifier %f\n",i,this.food.get(i));
        }



        System.console().printf("Predators modifier:\n");

        //Process through the predator array
        for(int i = 0 ; i < this.predaSize ; i++)
        {
            System.console().printf("Predator %d, modifier %f\n",i,this.preda.get(i));
        }
    

 
        System.console().printf("Disease:\n");

        //Process through the disease array by disease
        for(int i = 0 ; i < this.diseaseSize ; i++)
        {
            for(int j = 0 ; j < 3 ; j++)
            {
                System.console().printf("Disease %d, ",i);

                //Process through the array
                switch(j)
                {
                    //Print infection chance
                    case 0 :
                        System.console().printf("infection chance : %f\n",this.disease.get(i).get(j));
                        break;

                    //Print mortality chance on scattered population
                    case 1 :
                        System.console().printf("survivability on scattered population : %f\n",this.disease.get(i).get(j));
                        break;

                    //Print mortality chance on united population
                    case 2 :
                        System.console().printf("survivability on united population : %f\n",this.disease.get(i).get(j));
                        break;
                }
            }
        }


        System.console().printf("Climate:\n");

        //Process through the climate array by month
        for(int i = 0 ; i < this.climateSize ; i++)
        {

            System.console().printf("Month %d, ",i);

            //Process through the array
            for(int j = 0 ; j < 2 ; j++)
            {
                switch(j)
                {
                    //Print temperature
                    case 0 :
                        switch(this.climate.get(i).get(j))
                        {
                        case 0:
                            System.console().printf("cold and ");
                            break;
                        case 1 :
                            System.console().printf("moderate and ");
                            break;
                        case 2 :
                            System.console().printf("hot and ");
                            break;
                        }
                        break;

                    //Print humidity
                    case 1 :
                        switch(this.climate.get(i).get(j))
                        {
                        case 0:
                            System.console().printf("humid\n");
                            break;
                        case 1 :
                            System.console().printf("moderate\n");
                            break;
                        case 2 :
                            System.console().printf("dry\n");
                            break;
                        }
                        break;
                }
            }            
        } 
    }

    /*----------------------------------------------------------------------*
    * Getter foodSize                                                       *
    *                                                                       *
    * Output :                                                              *
    *      - an int representing the size of the food array                 *
    *----------------------------------------------------------------------*/
    public int foodSize()
    {
        return this.foodSize;
    }

    /*----------------------------------------------------------------------*
    * Setter foodSize                                                       *
    *                                                                       *
    * Input :                                                               *
    *      - _foodSize : an int representing the size of the food array     *
    *----------------------------------------------------------------------*/
    public void foodSize(int _foodSize)
    {
        this.foodSize = _foodSize;
    }

    /*----------------------------------------------------------------------*
    * Getter predaSize                                                      *
    *                                                                       *
    * Output :                                                              *
    *      - an int representing the size of the predator array             *
    *----------------------------------------------------------------------*/
    public int predaSize()
    {
        return this.predaSize;
    }

    /*----------------------------------------------------------------------*
    * Setter predaSize                                                      *
    *                                                                       *
    * Input :                                                               *
    *      - _predaSize : an int representing the size of the predator      *
    *        array                                                          *
    *----------------------------------------------------------------------*/
    public void predaSize(int _predaSize)
    {
        this.predaSize = _predaSize;
    }

    /*----------------------------------------------------------------------*
    * Getter climateSize                                                    *
    *                                                                       *
    * Output :                                                              *
    *      - an int representing the size of the climate array              *
    *----------------------------------------------------------------------*/
    public int climateSize()
    {
        return this.climateSize;
    }

    /*----------------------------------------------------------------------*
    * Setter climateSize                                                    *
    *                                                                       *
    * Input :                                                               *
    *      - _climateSize : an int representing the size of the climate     *
    *        array                                                          *
    *----------------------------------------------------------------------*/
    public void climateSize(int _climateSize)
    {
        this.climateSize = _climateSize;
    }

    /*----------------------------------------------------------------------*
    * Getter diseaseSize                                                    *
    *                                                                       *
    * Output :                                                              *
    *      - an int representing the size of the disease array              *
    *----------------------------------------------------------------------*/
    public int diseaseSize()
    {
        return this.diseaseSize;
    }

    /*----------------------------------------------------------------------*
    * Setter diseaseSize                                                    *
    *                                                                       *
    * Input :                                                               *
    *      - _diseaseSize : an int representing the size of the disease     *
    *        array                                                          *
    *----------------------------------------------------------------------*/
    public void diseaseSize(int _diseaseSize)
    {
        this.diseaseSize = _diseaseSize;
    }

    /*----------------------------------------------------------------------*
    * Getter surface                                                        *
    *                                                                       *
    * Output :                                                              *
    *      - a double representing the area surface                         *
    *----------------------------------------------------------------------*/
    public double surface()
    {
        return this.surface;
    }

    /*----------------------------------------------------------------------*
    * Setter surface                                                        *
    *                                                                       *
    * Input :                                                               *
    *      - _surface : a double representing the area surface              *
    *----------------------------------------------------------------------*/
    public void surface(double _surface)
    {
        this.surface = _surface;
    }

    /*----------------------------------------------------------------------*
    * addPreda : Add a modifier to the predator array                       *
    *                                                                       *
    * Input :                                                               *
    *      - _mod : a double representing the modifier                      *
    *----------------------------------------------------------------------*/
    public void addPreda(Double _mod)
    {
        this.preda.add(_mod);
    }

    /*----------------------------------------------------------------------*
    * addFood : Add a modifier to the food array                            *
    *                                                                       *
    * Input :                                                               *
    *      - _mod : a double representing the modifier                      *
    *----------------------------------------------------------------------*/
    public void addFood(Double _mod)
    {
        this.food.add(_mod);
    }

    /*----------------------------------------------------------------------*
    * addDisease : Add a modifier to the disease array                      *
    *                                                                       *
    * Input :                                                               *
    *      - _i : the index of the disease                                  *
    *      - _mod : a double representing the modifier                      *
    *----------------------------------------------------------------------*/
    public void addDisease(int _i, Double _mod)
    {
        this.disease.get(_i).add(_mod);
    }

    /*----------------------------------------------------------------------*
    * addClimate : Add a temperature/humidity state to the climate array    *
    *                                                                       *
    * Input :                                                               *
    *      - _i : the index of the month                                    *
    *      - _state : an integer representing the state                     *
    *----------------------------------------------------------------------*/
    public void addClimate(int _i, Integer _state)
    {
        this.climate.get(_i).add(_state);
    }

    /*----------------------------------------------------------------------*
    * getClimate : get the jth etlement from the ith month in climate array *
    *                                                                       *
    * Input :                                                               *
    *      - _i : the index of the month                                    *
    *      - _j : the index of the wanted element                           *
    *                                                                       *
    * Output :                                                              *
    *       - an int representing the target state                          *
    *----------------------------------------------------------------------*/
    public int getClimate(int _i, int _j)
    {
        return this.climate.get(_i).get(_j);
    }

    /*----------------------------------------------------------------------*
    * getFood : get the ith element from food array                         *
    *                                                                       *
    * Input :                                                               *
    *      - _i : the index                                                 *
    *                                                                       *
    * Output :                                                              *
    *       - an double representing the target modifier                    *
    *----------------------------------------------------------------------*/
    public double getFood(int _i)
    {
        return this.food.get(_i);
    }

    /*----------------------------------------------------------------------*
    * getPreda : get the ith element from predator array                    *
    *                                                                       *
    * Input :                                                               *
    *      - _i : the index                                                 *
    *                                                                       *
    * Output :                                                              *
    *       - an double representing the target modifier                    *
    *----------------------------------------------------------------------*/
    public double getPreda(int _i)
    {
        return this.preda.get(_i);
    }

    /*----------------------------------------------------------------------*
    * getDisease : get the jth etlement from the ith month in disease arry  *
    *                                                                       *
    * Input :                                                               *
    *      - _i : the index of the disease                                  *
    *      - _j : the index of the wanted element                           *
    *                                                                       *
    * Output :                                                              *
    *       - an double representing the target modifier                    *
    *----------------------------------------------------------------------*/
    public double getDisease(int i, int j)
    {
        return this.disease.get(i).get(j);
    }
}





